#include <stdio.h>
//#include <sys/types.h>
#include <unistd.h>


int 
main ( int argc, char **argv ) {

	for ( int i = 0; i < 5; i++ ) {
		if ( fork() )
			printf( "Im the parent\n" );
		else {
			printf( "%2d: Im one of many children\n", i );
			return 0;
		}
	}

	return 0;
}
