#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 8080
#define MAX_QUEUE 5


int
main ( int argc, char **argv ) {

	int server_fd, new_socket, valread;

	struct sockaddr_in address;

	int opt = 1;
	int addrlen = sizeof( address );
	char buffer[1024] = { 0 };
	char *hello = "Hello from server\n";

	// Create socket file descriptor
	// domain -> AF_INET ( IPv4 )
	// type -> SOCK_STREAM ( TCP )
	// protocol -> 0 ( IP )
	if (( server_fd = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP )) < 0 ) {
		fprintf( stderr, "socket failed" );
		exit( EXIT_FAILURE );
	}

	// Attach socket to PORT
	// optional
	if ( setsockopt( server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof( opt ) )) {
		fprintf( stderr, "setsockopt failed" );
		exit( EXIT_FAILURE );
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl( INADDR_ANY );
	address.sin_port = htons( PORT );


	// Binds socket to the desired address and port
	// sockfd -> used socket
	// struct *addr -> used address
	// addrlen -> address length
	if ( bind( server_fd, (struct sockaddr *) &address, sizeof( address ) ) < 0 ) {
		fprintf( stderr, "bind failed" );
		exit( EXIT_FAILURE );
	}

	// Wait for incomming connections
	// sockfd -> used socket
	// backlog -> max size of queue
	if ( listen( server_fd, MAX_QUEUE ) < 0 ) {
		fprintf( stderr, "listen failed" );
		exit( EXIT_FAILURE );
	}

	// Accepts connection and extracts client data
	// sockfd -> used socket
	// struct *addr -> used address
	// addrlen -> size of address
	if (( new_socket = accept( server_fd, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0 ) {
		fprintf( stderr, "accept failed" );
		exit( EXIT_FAILURE );
	}

	//valread = 
	recv( new_socket, buffer, 1024, 0 );
	printf( "Received message: %s\n", buffer );

	send( new_socket, hello, strlen( hello ), 0 );
	printf( "Message Sent\n" );

	return 0;
	
}
